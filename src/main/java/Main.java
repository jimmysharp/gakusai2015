import controller.CameraWindowController;
import controller.ConsoleWindowController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.MainModel;
import org.opencv.core.Core;

import java.nio.file.Paths;
import java.util.Map;

/**
 * Created by jimmy on 2015/10/14.
 */
public class Main extends Application{
    MainModel mainModel;

    public static void main(String[] args) {
        //画面表示
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        //Load OpenCV3.0.0
        System.load(Paths.get(System.getProperty("user.dir"), "lib", "opencv", "libopencv_java300.dylib").toString());
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        //モデル生成
        mainModel = new MainModel();

        FXMLLoader cameraWindowLoader = new FXMLLoader(getClass().getResource("CameraWindow.fxml"));
        Parent root = cameraWindowLoader.load();
        primaryStage.setTitle("カメラ");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
        CameraWindowController cameraWindowController = cameraWindowLoader.getController();
        cameraWindowController.connectModel(mainModel.getCameraModel());

        FXMLLoader consoleWindowLoader = new FXMLLoader(getClass().getResource("ConsoleWindow.fxml"));
        Stage consoleStage = new Stage();
        Parent consoleRoot = consoleWindowLoader.load();
        consoleStage.initModality(Modality.APPLICATION_MODAL);
        consoleStage.initOwner(primaryStage);
        consoleStage.setScene(new Scene(consoleRoot));
        consoleStage.show();
        ((ConsoleWindowController)consoleWindowLoader.getController()).connectModels(mainModel.getGamePadModel(),mainModel.getSerialModel(),mainModel.getLogModel());
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        mainModel.dispose();
        mainModel = null;
    }
}
