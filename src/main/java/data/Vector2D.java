package data;

/**
 * Created by jimmy on 2015/10/19.
 */
public class Vector2D {
    public float x;
    public float y;

    public Vector2D(float x, float y){
        this.x = x;
        this.y = y;
    }
}
