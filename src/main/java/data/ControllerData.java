package data;


import org.lwjgl.input.Controller;

/**
 * Created by jimmy on 2015/10/19.
 */
public class ControllerData {
    public static final int BUTTON_X = 13;
    public static final int BUTTON_Y = 14;
    public static final int BUTTON_A = 11;
    public static final int BUTTON_B = 12;
    public static final int BUTTON_BACK = 5;
    public static final int BUTTON_START = 4;
    public static final int BUTTON_LB = 8;
    public static final int BUTTON_L3 = 6;
    public static final int BUTTON_RB = 9;
    public static final int BUTTON_R3 = 7;
    public static final int BUTTON_UP = 0;
    public static final int BUTTON_DOWN = 1;
    public static final int BUTTON_LEFT = 2;
    public static final int BUTTON_RIGHT = 3;
    public static final int BUTTON_XBOX = 10;
    public static final int AXIS_LEFT_X = 0; //-1 is left | +1 is right
    public static final int AXIS_LEFT_Y = 1; //-1 is up | +1 is down
    public static final int AXIS_LEFT_TRIGGER = 4; //value 0 to 1f
    public static final int AXIS_RIGHT_X = 2; //-1 is left | +1 is right
    public static final int AXIS_RIGHT_Y = 3; //-1 is up | +1 is down
    public static final int AXIS_RIGHT_TRIGGER = 5; //value 0 to -1f

    public Vector2D rightStick;
    public Vector2D leftStick;

    public float triggerL;
    public float triggerR;

    public boolean buttonA;
    public boolean buttonB;
    public boolean buttonX;
    public boolean buttonY;
    public boolean buttonSTART;
    public boolean buttonBACK;
    public boolean buttonLB;
    public boolean buttonRB;
    public boolean buttonL3;
    public boolean buttonR3;
    public boolean buttonUP;
    public boolean buttonDOWN;
    public boolean buttonLEFT;
    public boolean buttonRIGHT;
    public boolean buttonXBOX;

    public ControllerData(Controller controller){
        buttonA = controller.isButtonPressed(BUTTON_A);
        buttonB = controller.isButtonPressed(BUTTON_B);
        buttonX = controller.isButtonPressed(BUTTON_X);
        buttonY = controller.isButtonPressed(BUTTON_Y);
        buttonSTART = controller.isButtonPressed(BUTTON_START);
        buttonBACK = controller.isButtonPressed(BUTTON_BACK);
        buttonLB = controller.isButtonPressed(BUTTON_LB);
        buttonRB = controller.isButtonPressed(BUTTON_RB);
        buttonL3 = controller.isButtonPressed(BUTTON_L3);
        buttonR3 = controller.isButtonPressed(BUTTON_R3);
        leftStick = new Vector2D(controller.getAxisValue(AXIS_LEFT_X), controller.getAxisValue(AXIS_LEFT_Y));
        rightStick = new Vector2D(controller.getAxisValue(AXIS_RIGHT_X), controller.getAxisValue(AXIS_RIGHT_Y));
        triggerL = controller.getAxisValue(AXIS_LEFT_TRIGGER);
        triggerR = controller.getAxisValue(AXIS_RIGHT_TRIGGER);
        buttonUP = controller.isButtonPressed(BUTTON_UP);
        buttonDOWN = controller.isButtonPressed(BUTTON_DOWN);
        buttonLEFT = controller.isButtonPressed(BUTTON_LEFT);
        buttonRIGHT = controller.isButtonPressed(BUTTON_RIGHT);
        buttonXBOX = controller.isButtonPressed(BUTTON_XBOX);
    }
}
