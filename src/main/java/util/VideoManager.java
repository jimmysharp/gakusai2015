package util;

import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;

/**
 * Created by jimmy on 2015/10/19.
 */
public class VideoManager {
    VideoCapture videoCapture;

    public VideoManager(){
        this(0);
    }
    public VideoManager(int device){
        videoCapture = new VideoCapture(device);
    }

    public Mat getImage(){
        Mat frame;
        if (videoCapture.isOpened()){
            frame = new Mat();
            videoCapture.read(frame);
            return frame;
        }
        return null;
    }

    public void dispose(){
        videoCapture.release();
        videoCapture = null;
    }
}
