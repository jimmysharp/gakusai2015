package controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import model.LogModel;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by jimmy on 2015/10/16.
 */
public class LogPaneController implements Observer{
    @FXML
    private TextArea ta_logMessage;

    @Override
    public void update(Observable o, Object arg) {
        LogModel logModel = (LogModel)o;
        for(String line : logModel.getMessage()){
            ta_logMessage.appendText(line+"\n");
        }
    }
}
