package controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import model.GamePadModel;
import model.LogModel;
import model.SerialModel;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by jimmy on 2015/10/19.
 */
public class ConsoleWindowController implements Initializable{
    @FXML
    private GamePadPaneController gamePadPaneController;
    @FXML
    private LogPaneController logPaneController;
    @FXML
    private SerialPaneController serialPaneController;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void connectModels(GamePadModel gamePadModel, SerialModel serialModel, LogModel logModel){
        serialPaneController.setSerialModel(serialModel);
        serialModel.addObserver(serialPaneController);
        logModel.addObserver(logPaneController);
        gamePadModel.addObserver(gamePadPaneController);
    }
}
