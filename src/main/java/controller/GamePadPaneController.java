package controller;

import data.ControllerData;
import data.Vector2D;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import model.GamePadModel;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by jimmy on 15/10/16.
 */
public class GamePadPaneController implements Observer{
    private final float AXIS_POINT_R = 5;

    @FXML
    public Canvas cv_leftStick;
    @FXML
    public Canvas cv_rightStick;
    @FXML
    public Circle cr_buttonX;
    @FXML
    public Circle cr_buttonY;
    @FXML
    public Circle cr_buttonB;
    @FXML
    public Circle cr_buttonA;
    @FXML
    public Ellipse el_buttonBACK;
    @FXML
    public Ellipse el_buttonSTART;
    @FXML
    public Rectangle rc_buttonRB;
    @FXML
    public Rectangle rc_buttonLB;
    @FXML
    public Rectangle rc_buttonLT;
    @FXML
    public Rectangle rc_buttonRT;
    @FXML
    public Circle cr_buttonXBOX;
    @FXML
    public Rectangle rc_buttonRIGHT;
    @FXML
    public Rectangle rc_buttonLEFT;
    @FXML
    public Rectangle rc_buttonUP;
    @FXML
    public Rectangle rc_buttonDOWN;

    @Override
    public void update(Observable o, Object arg) {
        GamePadModel gamePadModel = (GamePadModel)o;
        ControllerData padData = gamePadModel.getPadData();
        if(padData != null) setView(padData);
    }

    public void setView(ControllerData padData){
        GraphicsContext contextLS = cv_leftStick.getGraphicsContext2D();
        GraphicsContext contextRS = cv_rightStick.getGraphicsContext2D();

        //clear
        if (padData.buttonL3) contextLS.setFill(Color.GRAY);
        else contextLS.setFill(Color.WHITE);
        contextLS.clearRect(0,0,cv_leftStick.getWidth(),cv_leftStick.getHeight());
        contextLS.fillRect(0, 0, cv_leftStick.getWidth(), cv_leftStick.getHeight());
        if (padData.buttonR3) contextRS.setFill(Color.GRAY);
        else contextRS.setFill(Color.WHITE);
        contextRS.clearRect(0,0,cv_rightStick.getWidth(),cv_rightStick.getHeight());
        contextRS.fillRect(0, 0, cv_rightStick.getWidth(), cv_rightStick.getHeight());

        //grid
        contextLS.setStroke(Color.BLACK);
        contextLS.strokeLine(cv_leftStick.getHeight()/2,0,cv_leftStick.getHeight()/2,cv_leftStick.getWidth());
        contextLS.strokeLine(0,cv_leftStick.getWidth()/2,cv_leftStick.getHeight(),cv_leftStick.getWidth()/2);
        contextRS.setStroke(Color.BLACK);
        contextRS.strokeLine(cv_rightStick.getHeight()/2,0,cv_rightStick.getHeight()/2,cv_rightStick.getWidth());
        contextRS.strokeLine(0,cv_rightStick.getWidth()/2,cv_rightStick.getHeight(),cv_rightStick.getWidth()/2);

        renderPosition(contextLS, padData.leftStick, new Vector2D((float) cv_leftStick.getWidth(), (float) cv_leftStick.getHeight()));
        renderPosition(contextRS,padData.rightStick,new Vector2D((float)cv_rightStick.getWidth(),(float)cv_rightStick.getHeight()));

        if(padData.buttonX) cr_buttonX.setFill(Color.BLUE);
        else cr_buttonX.setFill(Color.WHITE);
        if(padData.buttonY) cr_buttonY.setFill(Color.YELLOW);
        else cr_buttonY.setFill(Color.WHITE);
        if(padData.buttonA) cr_buttonA.setFill(Color.GREEN);
        else cr_buttonA.setFill(Color.WHITE);
        if(padData.buttonB) cr_buttonB.setFill(Color.RED);
        else cr_buttonB.setFill(Color.WHITE);

        if(padData.buttonRB) rc_buttonRB.setFill(Color.BLACK);
        else rc_buttonRB.setFill(Color.WHITE);
        if(padData.buttonLB) rc_buttonLB.setFill(Color.BLACK);
        else rc_buttonLB.setFill(Color.WHITE);

        rc_buttonRT.setFill(getPercentageColor(1.0-getPercentage(padData.triggerR,-1.0,1.0)));
        rc_buttonLT.setFill(getPercentageColor(1.0-getPercentage(padData.triggerL,-1.0,1.0)));

        if(padData.buttonUP) rc_buttonUP.setFill(Color.BLACK);
        else rc_buttonUP.setFill(Color.WHITE);
        if(padData.buttonDOWN) rc_buttonDOWN.setFill(Color.BLACK);
        else rc_buttonDOWN.setFill(Color.WHITE);
        if(padData.buttonLEFT) rc_buttonLEFT.setFill(Color.BLACK);
        else rc_buttonLEFT.setFill(Color.WHITE);
        if(padData.buttonRIGHT) rc_buttonRIGHT.setFill(Color.BLACK);
        else rc_buttonRIGHT.setFill(Color.WHITE);

        if(padData.buttonBACK) el_buttonBACK.setFill(Color.BLACK);
        else el_buttonBACK.setFill(Color.WHITE);
        if(padData.buttonSTART) el_buttonSTART.setFill(Color.BLACK);
        else el_buttonSTART.setFill(Color.WHITE);
        if(padData.buttonXBOX) cr_buttonXBOX.setFill(Color.GREEN);
        else cr_buttonXBOX.setFill(Color.WHITE);
    }

    public double getPercentage(double value, double min, double max){
        return (value - min) / (max - min);
    }

    public Color getPercentageColor(double percentage){
        return Color.color(percentage,percentage,percentage);
    }

    public void renderPosition(GraphicsContext context, Vector2D vector, Vector2D size){
        context.setFill(Color.BLACK);
        context.fillOval(size.x * (vector.x + 1.0f) / 2.0 - AXIS_POINT_R, size.y * (vector.y + 1.0f) / 2.0 - AXIS_POINT_R, AXIS_POINT_R*2, AXIS_POINT_R*2);
    }
}
