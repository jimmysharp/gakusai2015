package controller;

import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import model.CameraModel;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by jimmy on 15/10/16.
 */
public class CameraWindowController implements Observer{
    @FXML
    private ImageView cameraView;

    public void connectModel(CameraModel cameraModel){
        cameraModel.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        CameraModel cameraModel = (CameraModel)o;

        Image image = cameraModel.getImage();
        if(image != null) cameraView.setImage(image);
    }
}
