package controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import model.SerialModel;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by jimmy on 15/10/16.
 */
public class SerialPaneController implements Observer{
    @FXML
    private ComboBox cb_portList;
    @FXML
    private Button bt_toggleSerial;
    @FXML
    private TextField tf_sendMessage;
    @FXML
    private Button bt_sendMessage;
    @FXML
    private Button bt_sendStop;

    private SerialModel serialModel;

    public void setSerialModel(SerialModel serialModel){
        this.serialModel = serialModel;
    }

    private void setView(){
        cb_portList.getItems().clear();
        for (String port : serialModel.getPortList().keySet()){
            cb_portList.getItems().add(port);
        }

        if (serialModel.isOpened()){
            cb_portList.setDisable(true);
            bt_toggleSerial.setText("停止");
            bt_toggleSerial.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    serialModel.close();
                }
            });
            tf_sendMessage.setDisable(false);
            bt_sendMessage.setDisable(false);
            bt_sendStop.setDisable(false);
        } else {
            cb_portList.setDisable(false);
            bt_toggleSerial.setText("接続");
            bt_toggleSerial.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    serialModel.open((String)cb_portList.getValue());
                }
            });
            tf_sendMessage.setDisable(true);
            bt_sendMessage.setDisable(true);
            bt_sendStop.setDisable(true);
        }
    }

    @FXML
    public void reloadPortList(){
        serialModel.updatePortList();
    }
    @FXML
    public void sendMessage() {
        String message = tf_sendMessage.getText();
        if (!message.equals("")) {
            serialModel.sendMessage(message, true);
            tf_sendMessage.setText("");
        }
    }

    @FXML
    public void sendStop() {
        serialModel.sendStop();
    }

    @Override
    public void update(Observable o, Object arg) {
        setView();
    }


}
