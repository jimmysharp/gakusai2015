package model;

import util.SerialManager;

import java.util.*;

/**
 * Created by jimmy on 2015/10/16.
 */
public class SerialModel extends Observable{
    //動作完了待機
    private boolean isWaiting;
    //シリアル通信
    private SerialManager serialManager;
    //シリアルポートリスト
    private Map<String,Boolean> portList;
    //メッセージキュー
    private LinkedList<String> messageQueue;

    private LogModel logModel;

    public SerialModel(LogModel logModel){
        this.logModel = logModel;
        messageQueue = new LinkedList<>();
        updatePortList();
    }

    public void open(String portName){
        boolean beforeOpened = this.isOpened();

        if (!beforeOpened) {
            logModel.addMessage("Serial port opening: "+portName);
            ResourceBundle bundle = ResourceBundle.getBundle("setting");
            serialManager = new SerialManager(portName, bundle.getString("app_name"));
        }

        if(beforeOpened != this.isOpened()){
            logModel.addMessage("Serial port opened: "+portName);
            setChanged();
            notifyObservers();
            isWaiting = false;
        } else {
            logModel.addMessage("Serial port open failed.");
        }
    }

    public void close(){
        if(serialManager != null && serialManager.isOpened()) {
            serialManager.close();
            serialManager = null;
            logModel.addMessage("Serial port closed.");

            setChanged();
            notifyObservers();
        }
    }

    public void sendMessage(String message, boolean isQueueing){
        if (serialManager != null && serialManager.isOpened()) {
            if (!isWaiting) {
                if (messageQueue.isEmpty()) {
                    serialManager.sendMessage(message);
                    logModel.addMessage("Send : "+message);
                } else {
                    String sendmessage = messageQueue.poll();
                    serialManager.sendMessage(sendmessage);
                    logModel.addMessage("Send : "+sendmessage);
                    if (isQueueing) messageQueue.add(message);
                }
            } else {
                if (isQueueing) messageQueue.add(message);
            }
        }
    }

    public void sendForward(int times){
        sendMessage("go"+String.valueOf(times),false);
        isWaiting = true;
    }
    public void sendForwardHalf(){
        sendMessage("goh",false);
        isWaiting = true;
    }

    public void sendRight(){
        sendMessage("right",false);
        isWaiting = true;
    }
    public void sendLeft(){
        sendMessage("left",false);
        isWaiting = true;
    }
    public void sendRightHalf(){
        sendMessage("righth",false);
        isWaiting = true;
    }
    public void sendLeftHalf(){
        sendMessage("lefth",false);
        isWaiting = true;
    }

    public void sendCrouch(){
        sendMessage("crouch",true);
        isWaiting = true;
    }

    public void sendArmLeft(){
        sendMessage("arml",true);
        isWaiting = true;
    }

    public void sendArmRight(){
        sendMessage("armr",true);
        isWaiting = true;
    }

    public void sendArmOpen(){
        sendMessage("armo",true);
        isWaiting = true;
    }

    public void sendArmExtend(){
        sendMessage("arme",true);
        isWaiting = true;
    }

    public void sendArmShorten(){
        sendMessage("arms",true);
        isWaiting = true;
    }

    public void sendDone(){
        sendMessage("done",true);
        isWaiting = false;
    }

    public void sendStop(){
        sendMessage("stop",true);
        isWaiting = false;
    }

    public void update(){
        if (serialManager != null) {
            for (String message : serialManager.getRecievedMessages()) {
                logModel.addMessage("Received: " + message);
                if (message.equals("done")) isWaiting = false;
            }
        }
    }

    public boolean isOpened(){
        if (serialManager == null) return false;
        else return serialManager.isOpened();
    }

    public boolean isWaiting(){
        return isWaiting;
    }

    public void updatePortList(){
        portList = SerialManager.getPorts();
        setChanged();
        notifyObservers();
    }

    public Map<String,Boolean> getPortList(){
        return portList;
    }

    public void dispose(){
        logModel = null;
        if(serialManager != null) serialManager.close();
        this.deleteObservers();
    }
}
