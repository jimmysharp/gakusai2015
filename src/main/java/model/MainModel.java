package model;

import data.ControllerData;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.util.Duration;

/**
 * Created by jimmy on 2015/10/19.
 */
public class MainModel {
    private static final float PAD_MARGIN = 0.2f;

    CameraModel cameraModel;
    GamePadModel gamePadModel;
    SerialModel serialModel;
    LogModel logModel;

    Timeline timer;

    public MainModel(){
        logModel = new LogModel();
        cameraModel = new CameraModel();
        gamePadModel = new GamePadModel();
        serialModel = new SerialModel(logModel);

        timer = new Timeline(new KeyFrame(
                Duration.millis(100),
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        update();
                    }
                }
        ));
        timer.setCycleCount(Timeline.INDEFINITE);
        timer.play();
    }

    public CameraModel getCameraModel(){
        return cameraModel;
    }
    public GamePadModel getGamePadModel(){
        return gamePadModel;
    }
    public SerialModel getSerialModel(){
        return serialModel;
    }
    public LogModel getLogModel(){
        return logModel;
    }

    public void update(){
        cameraModel.update();
        serialModel.update();
        gamePadModel.update();

        if (serialModel.isOpened() && !serialModel.isWaiting()){
            ControllerData padData = gamePadModel.getPadData();

            if (padData.leftStick.y < -PAD_MARGIN) serialModel.sendForward(1);
            else if (padData.leftStick.x < -PAD_MARGIN) serialModel.sendLeft();
            else if (padData.leftStick.x > PAD_MARGIN) serialModel.sendRight();
            else if (padData.buttonB) serialModel.sendCrouch();
            else if (padData.buttonLB) serialModel.sendArmLeft();
            else if (padData.buttonRB) serialModel.sendArmRight();
            else if (padData.buttonX) serialModel.sendArmOpen();
            else if (padData.triggerR > 0.0) serialModel.sendArmExtend();
            else if (padData.triggerL > 0.0) serialModel.sendArmShorten();
            else if (padData.buttonXBOX) serialModel.sendDone();

            if(padData.buttonSTART && padData.buttonBACK) serialModel.sendStop();
        }
    }

    public void dispose(){
        timer.stop();
        cameraModel.dispose();
        cameraModel = null;
        gamePadModel.dispose();
        gamePadModel = null;
        serialModel.dispose();
        serialModel = null;
        logModel.dispose();
        logModel = null;
    }
}
