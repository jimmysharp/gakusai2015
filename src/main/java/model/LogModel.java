package model;

import java.util.ArrayList;
import java.util.Observable;

/**
 * Created by jimmy on 2015/10/16.
 */
public class LogModel extends Observable {
    ArrayList<String> logMessage;

    public LogModel(){
        logMessage = new ArrayList<>();
    }

    public void addMessage(String message){
        logMessage.add(message);
        setChanged();
        notifyObservers();
    }

    public String[] getMessage(){
        String[] returnMessage = new String[logMessage.size()];
        logMessage.toArray(returnMessage);
        logMessage.clear();
        return returnMessage;
    }

    public void dispose(){
        this.deleteObservers();
        logMessage.clear();
        logMessage = null;
    }
}
