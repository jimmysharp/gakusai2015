package model;

import data.ControllerData;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Controller;
import org.lwjgl.input.Controllers;

import java.util.Observable;

/**
 * Created by jimmy on 2015/10/16.
 */
public class GamePadModel extends Observable{
    Controller controller;
    ControllerData padData;

    public GamePadModel(){
        try {
            Controllers.create();
        } catch (LWJGLException e) {
            e.printStackTrace();
        }
        Controllers.poll();
        controller = Controllers.getController(0);
    }

    public void update(){
        if (controller != null){
            controller.poll();
            padData = new ControllerData(controller);

            setChanged();
            notifyObservers();
        }
    }

    public ControllerData getPadData(){
        return padData;
    }

    public void dispose(){
        controller = null;
        this.deleteObservers();
    }
}
