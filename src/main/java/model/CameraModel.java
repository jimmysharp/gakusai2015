package model;

import javafx.scene.image.Image;
import org.opencv.core.Mat;
import util.ImageUtil;
import util.VideoManager;

import java.util.Observable;
import java.util.ResourceBundle;

/**
 * Created by jimmy on 2015/10/16.
 */
public class CameraModel extends Observable{
    VideoManager videoManager;
    Mat nowFrame;

    public CameraModel(){
        ResourceBundle bundle = ResourceBundle.getBundle("setting");

        videoManager = new VideoManager(Integer.parseInt(bundle.getString("camera_num")));
    }

    public void update(){
        Mat nextFrame = videoManager.getImage();
        if (nextFrame != null && !nextFrame.empty()){
            if(nowFrame != null) nowFrame.release();
            nowFrame = nextFrame;

            setChanged();
            notifyObservers();
        }
    }

    public Image getImage(){
        if (nowFrame != null && !nowFrame.empty()) return ImageUtil.matToImage(nowFrame);
        else return null;
    }

    public void dispose(){
        this.deleteObservers();
        videoManager.dispose();
    }
}
